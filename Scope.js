function example() {
    var x = "var"; // function scoped
    let y = "let"; // block scoped

    if (true) {
        var x = "var updated"; // re-declares and updates varVariable
        let y = "let updated"; // re-declares and updates letVariable in this block scope
        // const constVariable = "const updated"; // Error: constVariable has already been declared
    }

    console.log(x); // "var updated"
    console.log(y); // "let"
}

example();