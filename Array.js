

//Remove Duplicates
let numbers = [1, 2, 2, 3, 4, 4, 5];
numbers = [...new Set(numbers)]; //... converts Set object back to array
console.log(numbers); // [1, 2, 3, 4, 5]

