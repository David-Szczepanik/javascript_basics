const crypto = require('crypto');

function hashPassword(password) {
    const hash = crypto.createHash('sha256');
    hash.update(password);
    return hash.digest('hex');
}

// Usage
let password = 'myPassword';
let hashedPassword = hashPassword(password);

console.log("Hash:", hashedPassword); // Outputs: a hashed string



function hashPasswordSalt(password, salt) {
    return new Promise((resolve, reject) => {
        crypto.pbkdf2(password, salt, 100000, 64, 'sha256', (err, derivedKey) => {
            if (err) reject(err);
            resolve(derivedKey.toString('hex'));
        });
    });
}

// Usage
let password2 = 'myPassword';
let salt = crypto.randomBytes(16).toString('hex'); // Generate a random salt
hashPasswordSalt(password2, salt)
  .then(hashedPasswordSalt => {
      console.log("With salt:", hashedPasswordSalt); // Outputs: a hashed string
  })
  .catch(err => {
      console.error(err);
  });