//Primitive
// Number
// String
// Boolean
// Undefined
// Null
// Symbol
// BigInt

//Non-primitive
//Object
let obj = {
    name: "John",
    age: 30
};
console.log("Object: ", obj);

//Array
let arr = [1, 2, 3, 4, 5];
console.log("Array: ", arr);

//Function
function greet() {
    console.log("Function: Hello, World!");
}
greet();

//Date
let date = new Date();
console.log("Date: ", date);

//Regular Expression
let str = "Hello, World!";
let re = /World/;
let found = str.match(re); // match() method to search the string
console.log("RegExp: ", found);
console.log(found[0])

// Promise
let promise = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("Promise resolved after 5 seconds")
    }, 5000)});
promise.then((msg) => {
  console.log(msg)
})

console.log("\n--Map--")
//--Map--
let myMap = new Map();

//Key-Value pairs
//insertion order
myMap.set('key1', 'value1');
myMap.set('key2', 'value2');
myMap.set('key3', 'value3');

// Get a value by key
console.log(myMap.get('key1')); // Outputs: 'value1'

// Check if a key exists
console.log(myMap.has('key2')); // Outputs: true

// Get the size of the Map
console.log(myMap.size); // Outputs: 3

myMap.delete('key2');
console.log(myMap.has('key2'));
// Iterate over Map entries
myMap.forEach((value, key) => {
    console.log(key, value);
});

// Clear the Map
myMap.clear();



console.log("\n--WeakMap--")
//--Map--
//to associate Data with an Object
//can be garbage collected
let weakMap = new WeakMap();

// Create an object
let obj2 = {};

// Set a key-value pair
weakMap.set(obj2, 'value1');

// Get a value by key
console.log(weakMap.get(obj2)); // Outputs: 'value1'

// Check if a key exists
console.log(weakMap.has(obj2)); // Outputs: true

// Delete a key-value pair
weakMap.delete(obj2);
console.log(weakMap.has(obj2)); // Outputs: false

let set = new Set();
let weakSet = new WeakSet();
