let obj = { value: 1 };

function updateObject(o) {
    o.value = 2;
}

updateObject(obj);

console.log(obj.value); // Outputs: 2

